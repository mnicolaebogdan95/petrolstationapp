package ro.sdacademy;

import java.util.Scanner;

public class PetrolStation {

    public double gasolinePrice;
    public double dieselPrice;
    public double totalCost = 0.0;
    public final int REFUEL_WITH_GASOLINE = 0;
    public final int REFUEL_WITH_DIESEL = 1;

    public PetrolStation(double gasolinePrice, double dieselPrice) {
        this.gasolinePrice = gasolinePrice;
        this.dieselPrice = dieselPrice;
    }

    public void startProgram(Scanner scanner){
        chooseMenuOption(scanner);
    }


    public void showMenu() {
        System.out.println("1. Show price/L for gasoline");
        System.out.println("2. Show price/L for diesel");
        System.out.println("3. Refuel with gasoline");
        System.out.println("4. Refuel with diesel");
    }

    public void chooseMenuOption(Scanner scanner) {
        int chosenOption;
        char userAnswer;
        do {
            do {
                showMenu();
                System.out.print("Please choose an option: ");
                chosenOption = scanner.nextInt();
                switch (chosenOption) {
                    case 1:
                        showGasolinePrice();
                        break;
                    case 2:
                        showDieselPrice();
                        break;
                    case 3:
                        refuel(scanner,REFUEL_WITH_GASOLINE);
                        break;
                    case 4:
                        refuel(scanner,REFUEL_WITH_DIESEL);
                        break;
                    default:
                        System.out.println("Please choose a valid option!");
                }
            } while (chosenOption < 1 || chosenOption > 4);
            System.out.println("Do you want to choose another menu option? (Y/N)");
            scanner.nextLine();
            userAnswer = getUserAnswer(scanner);
            if (userAnswer == 'n'){
                sayGoodBye();
            }
        } while (userAnswer == 'y');
    }

    public char getUserAnswer(Scanner scanner) {
        char userAnswer;
        do {
            userAnswer = scanner.nextLine().toLowerCase().charAt(0);
            if (userAnswer != 'n' && userAnswer != 'y'){
                System.out.print("Please answer with y/n: ");
            }
        }while (userAnswer != 'n' && userAnswer != 'y');
        return userAnswer;
    }

    public void showGasolinePrice() {
        System.out.println("Price for 1L of gasoline is: " + this.gasolinePrice + " RON");
    }

    public void showDieselPrice() {
        System.out.println("Price for 1L of diesel is: " + this.dieselPrice + " RON");
    }

    public void refuel(Scanner scanner, int refuelType) {
        char userDecision = 'y';
        double refuelInLitre = 0.0;
        while (userDecision == 'y'){
            System.out.print("Enter amount of money you want to spent with refueling: ");
            double userResponse = scanner.nextDouble();
            if (refuelType == REFUEL_WITH_GASOLINE) {
                refuelInLitre += userResponse / gasolinePrice;
            }else {
                refuelInLitre += userResponse / dieselPrice;
            }
            System.out.println("You will refuel with " + refuelInLitre + " Litres");
            totalCost += userResponse;
            System.out.println("Do you want to add more Litres? (Y/N)");
            scanner.nextLine();
            userDecision = getUserAnswer(scanner);
        }
        askCustomerForPay(scanner);
    }

    public void askCustomerForPay(Scanner scanner){
        System.out.println("You have to pay " + totalCost + " RON");
        System.out.print("Enter amount of money to pay: ");
        double userEnteredAmount = scanner.nextDouble();
        handlePayment(userEnteredAmount);
    }

    public void handlePayment(double userEnteredAmount){
        Scanner scanner = new Scanner(System.in);
        if (userEnteredAmount < totalCost){
            double differenceMoney = totalCost - userEnteredAmount;
            do {
                System.out.println("You give me to less money, please give me more " + differenceMoney + " RON");
                System.out.print("Enter the remaining money to give: ");
                double givingMoney = scanner.nextDouble();
                differenceMoney = differenceMoney - givingMoney;
                if (differenceMoney < 0.0){
                    System.out.println("Here is your rest " + -differenceMoney + " RON");
                    totalCost = 0.0;
                    return;
                }else if (differenceMoney == 0.0){
                    System.out.println("Thank You!");
                    totalCost = 0.0;
                    return;
                }
            }while (differenceMoney != 0.0);
        }else  if (userEnteredAmount == totalCost){
            System.out.println("Thank you for chosen us!");
        }else {
            System.out.println("Here is your rest " + (userEnteredAmount - totalCost) + " RON");
        }
        totalCost = 0.0;
    }

    public void sayGoodBye(){
        System.out.println("Thank you for chosing us!");
        System.out.println("Have a nice day! :)");
    }

}
