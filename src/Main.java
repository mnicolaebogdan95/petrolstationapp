import ro.sdacademy.PetrolStation;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter what the gasoline price will be: ");
        double gasolinePrice = scanner.nextDouble();
        System.out.print("Enter what the diesel price will be: ");
        double dieselPrice = scanner.nextDouble();
        PetrolStation petrolStation = new PetrolStation(gasolinePrice, dieselPrice);
        petrolStation.startProgram(scanner);
    }
}
